<?php
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    "Avanti_MultiVendorsMundipagg",
    __DIR__
);
