<?php
declare(strict_types=1);

namespace Avanti\MultiVendorsMundipagg\Logger;

use Monolog\Logger;
use Magento\Framework\Logger\Handler\Base as HandlerBase;

class Handler extends HandlerBase
{
    protected $loggerType = Logger::NOTICE;
    protected $fileName = '/var/log/multivendors_mundipagg.log';
}
