<?php
declare(strict_types=1);

namespace Avanti\MultiVendorsMundipagg\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class MundipaggKeys extends AbstractFieldArray
{
    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender()
    {
        $this->addColumn('secret_key', ['label' => __('Secret Key'), 'class' => 'required-entry']);
        $this->addColumn('public_key', ['label' => __('Public key'), 'class' => 'required-entry']);
        $this->addColumn('cnpj', ['label' => __("VAT ID"), 'class' => 'required-entry']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }
}
