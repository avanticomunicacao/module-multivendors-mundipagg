<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Avanti\MultiVendorsMundipagg\Plugin\MundiPagg\MundiPagg\Gateway\Transaction\Base\Config;

use MundiPagg\MundiPagg\Gateway\Transaction\Base\Config\Config as ConfigAlias;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Avanti\MultiVendorsMundipagg\Logger\Logger;

class Config
{
    const XML_PATH_MUNDIPAGG_LAST_CREDENTIAL_USAGE = 'mundipagg_mundipagg/global/last_credential_usage';
    const PATH_TEST_MODE                           = 'mundipagg_mundipagg/global/test_mode';
    const XML_PATH_SANDBOX_CREDENTIALS             = 'avanti_mundipaggmultivendors/sandbox/mundipagg_keys';
    const XML_PATH_PRODUCTION_CREDENTIALS          = 'avanti_mundipaggmultivendors/production/mundipagg_keys';


    protected $scopeConfig;
    protected $logger;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Logger  $logger
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }

    public function afterGetSecretKey(ConfigAlias $subject, $result)
    {
        try {
            $lastTransactionKey = (int) $this->getLastTransactionKey();

            $this->logger->error("last transaction  id:" . $lastTransactionKey);

            $secretKey = $this->getSecretKey($lastTransactionKey);

            $this->logger->error("secret key: " . $secretKey);

            if (!$secretKey) {
                return $result;
            }

            return $secretKey;

        } catch (\Exception $e) {
            $this->logger->error("Um erro ocorreu ao tentar setar a chave privada da Mundipagg");
            $this->logger->error(print_r($e));
        }
    }

    public function afterGetPublicKey(ConfigAlias $subject, $result)
    {
        try {
            $lastTransactionKey = $this->getLastTransactionKey();
            $this->logger->error("last transaction_id: " . $lastTransactionKey);

            $publicKey = $this->getPublicKey($lastTransactionKey);

            $this->logger->error("public_key: " . $publicKey);

            return $publicKey ?? $result;
        } catch (\Exception $e) {
            $this->logger->error("Um erro ocorreu ao tentar setar a chave pública da Mundipagg");
            $this->logger->error(print_r($e));
        }
    }


    public function getLastTransactionKey()
    {
        $transactionKey = $this->getConfiguration(self::XML_PATH_MUNDIPAGG_LAST_CREDENTIAL_USAGE);

        $count = $this->countCredentials();

        if ($transactionKey != null) {
            $newTransactionKey = $transactionKey + 1;
            if ((int) $newTransactionKey >= $count) {
                return 0;
            } else {
                return $newTransactionKey;
            }
        }

        return 0;
    }

    private function getConfiguration($config)
    {
        $storeScope = ScopeInterface::SCOPE_WEBSITE;

        return $this->scopeConfig->getValue($config, $storeScope);
    }

    private function getTestMode()
    {
        return $this->getConfiguration(self::PATH_TEST_MODE);
    }

    private function countCredentials(): int
    {
        if ($this->getTestMode()) {
            $credentials = $this->getCredentials('sandbox');
            return count($credentials);
        }
        $credentials = $this->getCredentials();
        return count($credentials);
    }

    private function getCredentials($mode = 'production')
    {
        if ($mode == 'sandbox') {
            return json_decode($this->getConfiguration(self::XML_PATH_SANDBOX_CREDENTIALS), true);
        }

        return json_decode($this->getConfiguration(self::XML_PATH_PRODUCTION_CREDENTIALS), true);
    }

    private function getPublicKey($lastTransactionKey)
    {
        if ($this->getTestMode()) {
            $credentials = array_values($this->getCredentials('sandbox'));
            if (isset($credentials[$lastTransactionKey])) {
                return $credentials[$lastTransactionKey]['public_key'];
            }

            return null;
        }

        $credentials = $this->getCredentials();

        if (isset($credentials[$lastTransactionKey])) {
            return $credentials[$lastTransactionKey]['public_key'];
        }

        return null;
    }

    private function getSecretKey($lastTransactionKey)
    {
        if ($this->getTestMode()) {
            $credentials = array_values($this->getCredentials('sandbox'));
            if (isset($credentials[$lastTransactionKey])) {
                return $credentials[$lastTransactionKey]['secret_key'];
            }

            return null;
        }

        $credentials = $this->getCredentials();

        if (isset($credentials[$lastTransactionKey])) {
            return $credentials[$lastTransactionKey]['public_key'];
        }

        return null;
    }
}
