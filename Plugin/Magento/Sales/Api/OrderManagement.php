<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Avanti\MultiVendorsMundipagg\Plugin\Magento\Sales\Api;

use Avanti\MultiVendorsMundipagg\Logger\Logger;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Sales\Api\OrderRepositoryInterface;

class OrderManagement
{
    const XML_PATH_MUNDIPAGG_LAST_CREDENTIAL_USAGE = 'mundipagg_mundipagg/global/last_credential_usage';
    const PATH_TEST_MODE                           = 'mundipagg_mundipagg/global/test_mode';
    const XML_PATH_SANDBOX_CREDENTIALS             = 'avanti_mundipaggmultivendors/sandbox/mundipagg_keys';
    const XML_PATH_PRODUCTION_CREDENTIALS          = 'avanti_mundipaggmultivendors/production/mundipagg_keys';

    protected $scopeConfig;
    protected $logger;
    protected $configInterface;
    protected $cacheTypeList;
    protected $cacheFrontendPool;
    protected $historyFactory;
    protected $orderRepository;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Logger  $logger,
        ConfigInterface $configInterface,
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool,
        HistoryFactory  $historyFactory,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->configInterface = $configInterface;
        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
        $this->historyFactory = $historyFactory;
        $this->orderRepository = $orderRepository;
    }

    public function afterPlace(
        OrderManagementInterface $subject,
        $result
    ) {
        try {
            $lastTransactionId = $this->getLastTransactionKey();
            $cnpj = $this->getCnpj($lastTransactionId);

            $history = $this->historyFactory->create();
            $history->setStatus($result->getStatus());
            $history->setEntityName(Order::ENTITY);
            $history->setComment('MP - CNPJ da chave: ' . $cnpj);
            $history->setIsCustomerNotified(false);
            $history->setIsVisibleOnFront(false);

            $order = $this->orderRepository->get($result->getId());
            $order->addStatusHistory($history);
            $this->orderRepository->save($order);

            $this->setConfiguration(self::XML_PATH_MUNDIPAGG_LAST_CREDENTIAL_USAGE, $lastTransactionId);

            $types = [
                'config'
            ];

            foreach ($types as $type) {
                $this->cacheTypeList->cleanType($type);
            }
            foreach ($this->cacheFrontendPool as $cacheFrontend) {
                $cacheFrontend->getBackend()->clean();
            }

        } catch (\Exception $e) {
            $this->logger->error("Nao foi possível atualizar a última credencial usada. Erro informado: ");
            $this->logger->error(print_r($e));
        }

        return $result;
    }

    private function getCnpj($lastTransactionKey)
    {
        if ($this->getTestMode()) {
            $credentials = array_values(self::getCredentials('sandbox'));
            if (isset($credentials[$lastTransactionKey])) {
                return $credentials[$lastTransactionKey]['cnpj'];
            }

            return null;
        }
    }

    private function getConfiguration($config)
    {
        $storeScope = ScopeInterface::SCOPE_WEBSITES;
        return $this->scopeConfig->getValue($config, $storeScope, 1);
    }

    private function setConfiguration($config, $value)
    {
        $storeScope = ScopeInterface::SCOPE_WEBSITES;
        $this->configInterface->saveConfig($config, $value, $storeScope, 1);
    }

    public function getLastTransactionKey(): int
    {
        $transactionKey = $this->getConfiguration(self::XML_PATH_MUNDIPAGG_LAST_CREDENTIAL_USAGE);
        $count = $this->countCredentials();

        if ($transactionKey != null) {
            $newTransactionKey = $transactionKey + 1;
            if ((int) $newTransactionKey >= $count) {
                return 0;
            } else {
                return $newTransactionKey;
            }
        }

        return 0;
    }

    private function countCredentials(): int
    {
        if ($this->getTestMode()) {
            $credentials = $this->getCredentials('sandbox');
            return count($credentials);
        }
        $credentials = $this->getCredentials();
        return count($credentials);
    }

    private function getCredentials($mode = 'production')
    {
        if ($mode == 'sandbox') {
            return json_decode($this->getConfiguration(self::XML_PATH_SANDBOX_CREDENTIALS), true);
        }

        return json_decode($this->getConfiguration(self::XML_PATH_PRODUCTION_CREDENTIALS), true);
    }

    private function getTestMode()
    {
        return $this->getConfiguration(self::PATH_TEST_MODE);
    }
}
